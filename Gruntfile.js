/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Task configuration.
    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        unused: true,
        boss: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        }
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib_test: {
        src: ['lib/**/*.js', 'test/**/*.js']
      }
    },
    qunit: {
      files: ['test/**/*.html']
    },
    less: {
        development: {
            options: {
                sourceMap: true,
                dumpLineNumbers: true,
                sourceMapURL: '/styles/css/stny-filter.css.map',
                paths: "lib/components/bootstrap/less"
            },
            files: {
                'styles/css/app.css': 'styles/less/app.less'
            }
        }
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib_test: {
        files: '<%= jshint.lib_test.src %>',
        tasks: ['jshint:lib_test', 'qunit']
      },
      web: {
        options: {
            livereload: true
        },
        files: [
          '**/*.html', 
          '**/*.css', 
          '**/*.js',
          '../_simulator/**/*.js'
        ]
      },
      less: {
        options: {
            livereload: true
        },
        files: ['styles/less/*.less'],
        tasks: ['less']
      },
      bootstrap_less: {
        options: {
            livereload: true
        },
        files: ['lib/components/bootstrap/less/*.less'],
        tasks: ['less']
      }
    }
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-less');

  // Default task.
  grunt.registerTask('default', ['jshint', 'qunit']);

};
