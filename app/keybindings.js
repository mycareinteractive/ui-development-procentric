keybindings = function(){

    jQuery(document).on('keypress', 'body', function(e){
        var kc = e.which;
        var hkc = hcap.key.Code;

        debug.status("Key Code: " + kc);

        switch(kc){
            case hkc.PLAY:  
                rtsp_play();
                //debug.status("Socket ID: " + socket.id);
                //debug.status("Socket Connected: " + socket.connected);
                //debug.status("We pressed the PLAY button...");
                //debug.status("Is Procentric Object loaded? " + window.LG.getIsLoaded());
                //debug.status("LG IP Address: " + window.LG.getIp());
                // IP channel class 2 change request
                hcap.channel.requestChangeCurrentChannel({
                     "channelType":hcap.channel.ChannelType.IP, 
                     "ip": "192.168.100.158",
                     "port":1235,
                     "ipBroadcastType":hcap.channel.IpBroadcastType.UDP, 
                     "onSuccess":function() {
                         debug.status("PLAY VIDEO: onSuccess");
                         $('body').hide();
                     }, 
                     "onFailure":function(f) {
                         debug.status("onFailure : errorMessage = " + f.errorMessage);
                     }
                });
                break;

            case hkc.RED:
                location.href = '/init.lab.php';
                break;

            case hkc.GREEN:
                hcap.mode.getHcapMode({
                    onSuccess: function(s){
                        debug.status('HCAP MODE: ' + s.mode);
                    },
                    onFailure: function(f){}
                });
                break;

            case hkc.BLUE:
                $('body').toggle();
                break;
            case hkc.MENU:

            case hkc.PORTAL:
                hcap.mode.setHcapMode({
                     mode: hcap.mode.HCAP_MODE_1,
                     onSuccess: function() {
                         console.log("onSuccess");
                     }, 
                     onFailure: function(f) {
                         console.log("onFailure : errorMessage = " + f.errorMessage);
                     }
                });
                break;

            case hkc.EXIT:
                hcap.mode.setHcapMode({
                     mode: hcap.mode.HCAP_MODE_0,
                     onSuccess: function() {
                         console.log("onSuccess");
                     }, 
                     onFailure: function(f) {
                         console.log("onFailure : errorMessage = " + f.errorMessage);
                     }
                });
                break;
        }
    });

}