Procentric = function(){
    this.loadTotal = 6; // how many properties should load
    this.countLoaded = 0; // how many properties are currently loaded
    this.errorCount = 0;
    this.errors = new Array();
    this.successCount = 0;
    this.isLoaded = false;

    this.modelName = "";
    this.platformVersion = "";
    this.platform = "";
    this.version = "PROCENTRIC";
    this.mac = "";
    this.ip = "";
    this.devices = "";
    this.timeZoneName = "";
    this.applicationList = new Array();
    this.networkDeviceCount = "";
    this.networkDeviceList = "";
}

Procentric.prototype.bootstrap = function(success) {
    debug.status('Bootstrapping Procentric');
    this.fetchModelName();
    this.fetchMac();
    this.fetchIp();
    this.fetchTimeZoneName();
    this.fetchApplicationList();
    this.fetchNetworkDeviceCount();

    this.successCallback = success;

    this.load(this); 
}

Procentric.prototype.load = function(scope){
    //debug.status('Load Count: ' + this.countLoaded + ' Success Count: ' + this.successCount + ' Error Count: ' + this.errorCount);

    if(scope.getIsLoaded() == false){
        //debug.status('Checking for isLoaded');
        setTimeout(function(){
            scope.load(scope)
        }, 1000);
    } else {
        scope.successCallback();
        debug.status('BOOTSTRAPPED!');
        debug.status("Load Total: " + scope.loadTotal);
        debug.status("Loaded: " + scope.countLoaded);
    }
}

Procentric.prototype.setCountLoaded = function(){
    this.countLoaded = this.errorCount + this.successCount;
    if(this.countLoaded >= this.loadTotal){
        this.isLoaded = true;
        debug.status('All dependencies have been loaded');
    }
    debug.status('Count Loaded: ' + this.countLoaded);
}

Procentric.prototype.getIsLoaded = function(){
    return this.isLoaded;
}

Procentric.prototype.setSuccessCount = function (){
    this.successCount++;
    this.setCountLoaded();
}

Procentric.prototype.setErrorCount = function(){
    this.errorCount++;
    this.setCountLoaded();
}

Procentric.prototype.setError = function(method, message){
    this.errors.push({
        method: method,
        message: message
    });
}

Procentric.prototype.getError = function(){
    return this.errors;
}

Procentric.prototype.setModelName = function(res){
    this.platform = res.value;
    this.platformVersion = res.value;
    this.countLoaded = this.countLoaded + 1;
    debug.status('Platform: ' + this.platform);
};

Procentric.prototype.setMac = function(res){
    this.mac = res.mac;
    this.setSuccessCount();
    debug.status('MAC Address: ' + this.mac);
};

Procentric.prototype.setIp = function(res){
    this.ip = res.ip_address;
    this.setSuccessCount();
    debug.status("IP Address: " + this.ip);
};

Procentric.prototype.setNetworkDeviceCount = function(res){
    this.networkDeviceCount = res.count;
    this.setSuccessCount();
    debug.status('Net Device Count: ' + this.networkDeviceCount);
};

// TODO setDeviceList
Procentric.prototype.setDeviceList = function(){
    this.setSuccessCount();
};

// TODO setConnectedDevices
Procentric.prototype.setConnectedDevices = function(){
    this.setSuccessCount();
};

Procentric.prototype.setNetworkDevice = function(res){
    if(typeof this.networkDeviceList !== "array")
        this.networkDeviceList = new Array();

    if(typeof res === 'object' && res !== null) {
        try{
            this.networkDeviceList.push(res);
            this.setSuccessCount();
        } catch(e) {
            debug.status(e);
        }
        debug.status("Network Mode: " + res.networkMode + " Name: " + res.name + " MAC: " + res.mac);
    }
};

Procentric.prototype.setTimeZoneName = function(res){
    this.timeZoneName = res.gmtOffsetInMinute;
    this.setSuccessCount();
    debug.status("Time Zone: " + this.timeZoneName);
};

Procentric.prototype.setApplicationList = function(res){
    this.applicationList = res.list;

    var li = [];
    var ul = $("<ul />");
    for(var i = 0; i < res.list.length; i++){
        var inf = $("<li />").append([res.list[i].id, res.list[i].title, res.list[i].iconFilePath].join(','));
        ul.append(inf);
    }
    this.setSuccessCount();
    debug.status(ul);
};

Procentric.prototype.fetchModelName = function(){
    var self = this;
    hcap.property.getProperty({
        key: "model_name",
        onSuccess: function(res){
            self.setModelName(res);
        },
        onFailure: function(res){
            self.setErrorCount();
            self.setError('fetchModelName', res.errorMessage);
        }
    });
};

Procentric.prototype.fetchMac = function(){
    var self = this;
    debug.status('Fetching MAC Address');
    hcap.network.getNetworkDevice({
        index:0,
        onSuccess:function(res) {
            self.setMac(res);
        },
        onFailure: function(res){
            self.setErrorCount();
            self.setError('fetchMac', res.errorMessage);
        }
    });
};

Procentric.prototype.fetchIp = function(){
    var self = this;
    debug.status('Fetching IP Address');
    hcap.network.getNetworkInformation({
        onSuccess: function(res){
            self.setIp(res);
        },
        onFailure: function(res){
            self.setErrorCount();
            self.setError('fetchIp', res.errorMessage);
        }
    });
};

Procentric.prototype.fetchNetworkDeviceCount = function(){
    var self = this;
    debug.status('Fetching Network Device Count');
    hcap.network.getNumberOfNetworkDevices({
        onSuccess: function(res){
            self.setNetworkDeviceCount(res);
            self.fetchNetworkDeviceList();
        },
        onFailure: function(res){
            self.setErrorCount();
            self.setError('fetchNetworkDeviceCount', res.errorMessage);
        }
    });
};

Procentric.prototype.fetchNetworkDeviceList = function(){
    var self = this;
    var count = self.networkDeviceCount;
    if(count > 0){
        for(var i = 0; i < count; i++){
            self.fetchNetworkDevice(i);
        }
    }
};

Procentric.prototype.fetchNetworkDevice = function(index){
    var self = this;
    debug.status('Fetching Network Device');
    hcap.network.getNetworkDevice({
        index: index,
        onSuccess: function(res){
            self.setNetworkDevice(res);
        },
        onFailure: function(res){
            self.setErrorCount();
            self.setError('fetchNetworkDevice', res.errorMessage);
        }
    });
};

// TODO fetchConnectedDevices
Procentric.prototype.fetchConnectedDevices = function(){
    var self = this;
};

Procentric.prototype.fetchTimeZoneName = function(){
    var self = this;
    debug.status('Fetching Timezone From TV');
    hcap.time.getLocalTime({
        onSuccess:function(res) {
            self.setTimeZoneName(res);
        },
        onFailure: function(res){
            self.setErrorCount();
            self.setError('fetchTimeZoneName', res.errorMessage);
        }
    });
};

Procentric.prototype.fetchApplicationList = function(){
    var self = this;
    debug.status('Fetching Application List');
    hcap.preloadedApplication.getPreloadedApplicationList({
        onSuccess: function(res){
            self.setApplicationList(res);
        },
        onFailure: function(res){
            self.setErrorCount();
            self.setError('fetchApplicationList', res.errorMessage);
        }
    });
};

Procentric.prototype.executeBrowser = function(){};

Procentric.prototype.getModelName = function(){
    return this.modelName;
}

Procentric.prototype.getPlatformVersion = function(){
    return this.platformVersion;
}

Procentric.prototype.getPlatform = function(){
    return this.platform;
}

Procentric.prototype.getVersion = function(){
    return this.version;
}

Procentric.prototype.getMac = function(){
    return this.mac;
}

Procentric.prototype.getIp = function(){
    return this.ip;
}

Procentric.prototype.getDevices = function(){
    return this.devices;
}

Procentric.prototype.getTimeZoneName = function(){
    return this.timeZoneName;
}

Procentric.prototype.getApplicationList = function(){
    return this.applicationList;
}

Procentric.prototype.getNetworkDeviceCount = function() {
    return this.networkDeviceCount;
}

Procentric.prototype.getNetworkDeviceList = function(){
    return this.networkDeviceList;
}