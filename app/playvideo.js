//socket.emit('handshake', '192.168.100.60:1235', 'xxxxxxx', '101');
//socket.emit('handshake', ipaddress, 'xxxxxxx', '101');
function playvideo(){

    var socket = io('http://192.168.100.151:1337');
    window.socket = socket;
    ipaddress = '192.168.100.158:1235';
    socket.emit('handshake', ipaddress, 'xxxxxxx', '101');

    socket.on('message', function (data) {
        console.log(data);
        $('<li></li>').text(data).appendTo('#messages');
    });

    socket.on('handshake', function () {
        loadStream();
    });

    socket.on('ended', function (data) {
        console.log(data);
        $('<li></li>').text('stream ended').appendTo('#messages');
    });

    socket.on('loaded', function (data) {
        $('<li></li>').text(JSON.stringify(data)).appendTo('#messages');
        playStream(1, "0.0-");
    });

    socket.on('playing', function (data) {
        $('<li></li>').text(data).appendTo('#messages');
    });

    socket.on('paused', function (data) {
        $('<li></li>').text(data).appendTo('#messages');
    });

    socket.on('rcvdParams', function (data) {
        $('<li></li>').text(data).appendTo('#messages');
    });

    socket.on('announced', function (code, data) {
        $('<li></li>').text(code + ": " + data).appendTo('#messages');
    });

    socket.on('teardown', function (code, data) {
        $('<li></li>').text(code + ": " + data + " we toh it dizown").appendTo('#messages');
    });

    debug.status('READY TO PLAY SOME VIDEO!!!');
};

function handshake(ip) {
    socket.emit('handshake', ip, 'xxxxxxx', '101');
}

function loadStream() {
    socket.emit('load',
       'rtsp://192.168.100.80:554/60010000?assetUid=3b9acb15&transport=MP2T/AVP/UDP&ServiceGroup=100&smartcard-id=0021F80337F2&device-id=0021F80337F2&home-id=100326_1&client_mac=0021F80337F2')
}

function playStream(scale, offset) {
    socket.emit('play', scale, offset);
}

function pauseStream() {
    socket.emit('pause');
}

function getCurrentTime() {
    socket.emit('currentTime');
}

function teardown() {
    socket.emit('teardown');
}

function rtsp_play() {
    debug.status('!!!!PLAY STREAM!!!');
    playStream();
}

function rtsp_pause() {
    pauseStream();
}

function rtsp_getCurrentTime() {
    getCurrentTime();
}

function rtsp_teardown() {
    teardown();
}

function rtsp_handshake(ip) {
    $('<li></li>').text("Restarting stream and sending to: " + ip).appendTo('#messages');
    handshake(ip);
}